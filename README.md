#  Sixt - Coding Challenge on 01.12.2019

## Important !!!

Here by I acknowledge that all the characters in the source codes are generated and typed by me and it is a 100% work completed by my own.

## Usage of external dependencies

None of the external dependencies are used.

## Assumptions

I assumed that the App's minimum deployment target is iOS 13 and Swift 5.1.

The App assums that the user is at `static let munichLocation = CLLocation(latitude: 48.1351, longitude: 11.5820)` In the reality CLLocationManager has to be used to obtain the current locaion, but the given static JSON contains static location around Munich area. There for the distance calculation should give some realsitic data.

Simulator --> Debug --> Location --> Custom loction --> (latitude: 48.1351, longitude: 11.5820)

## Known issues

0. The design is tested only on iPhone X.
1. A simple filter function is implemented just as a demonstrate to the functional part of the App.
2. No data persistence such as Core Data or SQLite.
3. No Custom UI, simply the iOS built-in UI is used.
4. No comments used, because code speaks better than words.
