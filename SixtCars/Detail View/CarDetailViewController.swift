//
//  CarDetailViewController.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 27.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

import MapKit

class CarDetailViewController: UIViewController {

    var car: Sixt.Car! // Must be there
    
    var imageCacheManager: CarViewModel.ImageCacheManager?
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var baseView: UIView!
        
    @IBOutlet weak var previewImageView: UIImageView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelNameLabel: UILabel!

    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var transmissionTypeLabel: UILabel!
    @IBOutlet weak var innerCleanlinessLabel: UILabel!
    
    @IBOutlet weak var nameImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var licensePlateLabel: UILabel!
    
    @IBOutlet weak var bookNowButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = car.description
        
        baseView.layer.cornerRadius = 16
        
        baseView.setShadow()
        
        bookNowButton.layer.cornerRadius = bookNowButton.frame.height/2
        
        nameImageView.layer.cornerRadius = 8
        
        mapView.addAnnotation(CarAnnotation(car: car))
        
        prepareMap()
        
        prepareUI()
    }
    
    fileprivate func prepareMap() {
        
        let request = MKDirections.Request()
        
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: Sixt.munichLocation.coordinate, addressDictionary: nil))
        
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: car.location.coordinate, addressDictionary: nil))
        
        request.requestsAlternateRoutes = true
        request.transportType = .automobile

        let directions = MKDirections(request: request)
//
        directions.calculate { [unowned self] response, error in
            
            guard let unwrappedResponse = response else { return }

            for route in unwrappedResponse.routes {
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 100, right: 50), animated: true)
                
            }
        }
    }
    
    fileprivate func prepareUI() {
        
        distanceLabel.text = car.distanceLabel
        makeLabel.text = car.make
        modelNameLabel.text = car.modelName
        
        imageCacheManager?.loadImage(for: car) { [weak self] image in
            
            self?.previewImageView.image = image
        }
        
        colorLabel.text = car.color.replacingOccurrences(of: "_", with: " ").capitalized
        fuelTypeLabel.text = "\(car.fuelType.description) (\(Int(car.fuelLevel * 100))%)"
        transmissionTypeLabel.text = car.transmissionType.description
        innerCleanlinessLabel.text = car.innerCleanlinessType.description
        
        nameLabel.text = car.name
        licensePlateLabel.text = car.licensePlate
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CarDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        
        renderer.strokeColor = #colorLiteral(red: 0.04380319268, green: 0.4975004196, blue: 0.9978448749, alpha: 1)
        
        return renderer
    }
}
