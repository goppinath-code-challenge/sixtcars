//
//  Double+Formatter.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 24.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

extension Double {
    
    var metersToKilometers: Double {
        
        return Measurement(value: self, unit: UnitLength.meters)
            .converted(to: .kilometers)
            .value
    }
}

extension Formatter {
    
    static let measurement: MeasurementFormatter = {
        
       let formatter = MeasurementFormatter()
        formatter.locale = Locale.current
        formatter.unitOptions = .providedUnit
        formatter.numberFormatter.maximumFractionDigits = 1
        formatter.unitStyle = .short
        
        return formatter
    }()
}
