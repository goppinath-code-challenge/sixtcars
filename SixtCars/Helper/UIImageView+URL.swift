//
//  UIImageView+URL.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 24.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func load(url: URL, defaultImage: UIImage? = nil, completion: ((UIImage?) -> ())? = nil) {
        
        URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            
            if let data = data {
                
                if let image = UIImage(data: data) {
                    
                    DispatchQueue.main.async { self.image = image }
                    completion?(image)
                }
                else {
                    
                    // TODO: What if the data cannot be converted to UIImage
                    DispatchQueue.main.async { self.image = defaultImage }
                    completion?(nil)
                }
            }
            else {
                
                // TODO: Handle network errors
                
                DispatchQueue.main.async { self.image = defaultImage }
                completion?(nil)
            }
        }.resume()
    }
}
