//
//  CarMapViewController.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 23.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

import MapKit

class CarMapViewController: UIViewController {

    let showCarDetailViewControllerSegue = "showCarDetailViewControllerSegue"
    
    let visibleMeters: CLLocationDistance = 10000
    
    @IBOutlet fileprivate var viewModel: CarViewModel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    
    fileprivate func prepareCurrentRegion() {
        
        self.mapView.setRegion(MKCoordinateRegion(center: Sixt.munichLocation.coordinate, latitudinalMeters: visibleMeters, longitudinalMeters: visibleMeters), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager.requestWhenInUseAuthorization()
        
        prepareCurrentRegion()
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        viewModel.loadCars {
            
            DispatchQueue.main.async { [weak self] in
            
                if let strongSelf = self {
                    
                    strongSelf.mapView.removeAnnotations(strongSelf.mapView.annotations)
                    
                    strongSelf.mapView.addAnnotations(strongSelf.viewModel.pointAnnotations())
                }
            }
        }
    }
    
}

extension CarMapViewController {
    
    @IBAction func locationBBITapped(_ sender: UIBarButtonItem) {
        
        prepareCurrentRegion()
    }
}

extension CarMapViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let car = (sender as? CarAnnotation)?.car else { return }
        
        switch segue.destination {
        case let carDetailViewController as CarDetailViewController:
            
            carDetailViewController.car = car
            carDetailViewController.imageCacheManager = viewModel.imageCacheManager
            
        default: break
        }
    }
}

extension CarMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        guard let carAnnotation = view.annotation as? CarAnnotation else { return }
        
        performSegue(withIdentifier: showCarDetailViewControllerSegue, sender: carAnnotation)
    }
}
