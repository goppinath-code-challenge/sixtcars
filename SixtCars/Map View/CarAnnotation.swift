//
//  CarAnnotation.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 28.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

import MapKit

class CarAnnotation: MKPointAnnotation {

    let car: Sixt.Car
    
    init(car: Sixt.Car) {
        
        self.car = car
        
        super.init()
        
        coordinate = car.location.coordinate
        
        title = car.description
        subtitle = car.name
    }
}
