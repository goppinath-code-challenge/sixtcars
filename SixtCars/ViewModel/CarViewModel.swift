//
//  CarViewModel.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 24.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation
import UIKit

class CarViewModel: NSObject {
    
    private var cars = [Sixt.Car]()
    
    private var filteredCars = [Sixt.Car]()
    
    lazy var imageCacheManager = ImageCacheManager()
    
    var carFilter: CarFilter? {
        
        willSet {
            
            apply(filter: newValue)
        }
    }
    
    func apply(filter: CarFilter?) {
        
        guard let filter = filter else { filteredCars = cars; return }
        
        filteredCars = cars.filter { $0.passing(filter: filter) }
    }
    
    func loadCars(completion: (() -> Void)? = nil) {
        
        Sixt.getCars { [weak self] (result) in
            
            switch result {
            case .failure(let error): print(error)
            case .success(let cars): self?.cars = cars.sorted(by: { $0.distance < $1.distance })
            }
            
            self?.apply(filter: self?.carFilter)
            
            completion?()
        }
    }
}

extension CarViewModel {
    
    class ImageCacheManager {
        
        private lazy var carImageCache = NSCache<NSString, UIImage>()
        
        private func load(imageURL url: URL,  completion: @escaping (UIImage?) -> ()) {
            
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                
                if let data = data {
                    
                    if let image = UIImage(data: data) {
                
                        completion(image)
                    }
                    else {
                        
                        // TODO: What if the data cannot be converted to UIImage
                        completion(nil)
                    }
                }
                else {
                    
                    // TODO: Handle network errors
                    completion(nil)
                }
            }.resume()
        }
        
        func loadImage(for car: Sixt.Car, completion: @escaping (UIImage) -> ()) {
            
            if let carImage = carImageCache.object(forKey: car.id as NSString) {
                
                 completion(carImage)
            }
            else {
                
                load(imageURL: car.imageURL) { [weak self] image in
                    
                    DispatchQueue.main.async { completion(image ?? #imageLiteral(resourceName: "vehicle-placeholder")) }
                    
                    if let image = image { self?.carImageCache.setObject(image, forKey: car.id as NSString) }
                }
            }
        }
    }
}

// MARK:- Table View DataSource
extension CarViewModel {
    
    func numberOfSections() -> Int {
        
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        
        return filteredCars.count
    }
    
    func car(at: IndexPath) -> Sixt.Car {
        
        return filteredCars[at.row]
    }
}

import MapKit

extension CarViewModel {

    @objc func pointAnnotations() -> [MKPointAnnotation] {
        
        return filteredCars.map { CarAnnotation(car: $0) }
    }
}
