//
//  CarFilter.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 30.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

class CarFilter {
    
//    var transmissionTypeSet = Set([Sixt.Car.TransmissionType.automatic, Sixt.Car.TransmissionType.manual])
//
//    var fuelTypeSet = Set([Sixt.Car.FuelType.petrol, Sixt.Car.FuelType.diesel, Sixt.Car.FuelType.electric])
    
    var transmissionTypeSet = Set<Sixt.Car.TransmissionType>()
    
    var fuelTypeSet = Set<Sixt.Car.FuelType>()
}
