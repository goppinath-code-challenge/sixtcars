//
//  Car.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 23.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation
import UIKit

import CoreLocation

extension Sixt {
    
    // CLLocationManager has to be used to obtain the current locaion, but the given static JSON contains static location around Munich area. There for the distance calculation shold give some realsitic data.
    static let munichLocation = CLLocation(latitude: 48.1351, longitude: 11.5820)
    
    struct Car: Codable {
        
        enum FuelType: String, Codable {
            
            case petrol = "P"
            case diesel = "D"
            case electric = "E"
            
            var description: String {
                
                switch self {
                case .petrol:   return "Petrol"
                case .diesel:   return "Diesel"
                case .electric: return "Electric"
                }
            }
            
            var color: UIColor {
                
                switch self {
                case .petrol:   return #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                case .diesel:   return #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                case .electric: return #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                }
            }
        }
        
        enum TransmissionType: String, Codable {
            
            case automatic = "A"
            case manual = "M"
            
            var description: String {
                
                switch self {
                case .automatic:    return "Automatic"
                case .manual:       return "Manual"
                }
            }
            
            var iconImageName: String {
                
                switch self {
                case .automatic:    return "a.circle" 
                case .manual:       return "m.circle"
                }
            }
        }
        
        enum CleanlinessType: String, Codable {
            
            case regular = "REGULAR"
            case clean = "CLEAN"
            case veryClean = "VERY_CLEAN"
            
            var description: String {
                
                switch self {
                case .regular:      return "Regular"
                case .clean:        return "Clean"
                case .veryClean:    return "Very clean"
                }
            }
        }
        
        let id, modelIdentifier, modelName, name: String
        let make, group, color, series: String
        let fuelType: FuelType
        let fuelLevel: Double
        let transmissionType: TransmissionType
        let licensePlate: String
        let latitude, longitude: Double
        let innerCleanlinessType: CleanlinessType
        let imageURL: URL

        enum CodingKeys: String, CodingKey {
            case id, modelIdentifier, modelName, name, make, group, color, series, fuelType, fuelLevel, licensePlate, latitude, longitude
            case innerCleanlinessType = "innerCleanliness"
            case transmissionType = "transmission"
            case imageURL = "carImageUrl"
        }
    }
}


extension Sixt.Car {
    
    var description: String {
        
        return "\(make) \(modelName)"
    }
    
    var location: CLLocation {
         
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    var distance: Double {
        
        return Sixt.munichLocation.distance(from: location)
    }
    
    var distanceLabel: String {
        
        Formatter.measurement.string(from: Measurement(value: distance.metersToKilometers, unit: UnitLength.kilometers).converted(to: .kilometers))
    }
}

extension Sixt.Car {

    func passing(filter: CarFilter) -> Bool {
        
        return filter.transmissionTypeSet.contains(transmissionType) && filter.fuelTypeSet.contains(fuelType)
    }
}
