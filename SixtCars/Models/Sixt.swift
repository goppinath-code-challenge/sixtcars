//
//  Sixt.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 23.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

struct Sixt {
    
    enum ErrorType: Error {
        
        case unknown
    }
    
    static var url: URL {
        
        // https://cdn.sixt.io/codingtask/cars
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "cdn.sixt.io"
        urlComponents.path = "/codingtask"
        
        return urlComponents.url! // It must be there
    }
    
    enum RESTMethod: String {
        
        case getCars = "cars"
        
        var url: URL { return Sixt.url.appendingPathComponent(self.rawValue) }
    }
}

extension Sixt {
    
    static func getCars(completion: @escaping (Result<[Car], Error>) -> Void) {
        
        let urlRequest = URLRequest(url: Sixt.RESTMethod.getCars.url)
        
        URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            guard let data = data else { completion(.failure(error ?? ErrorType.unknown)); return }
            
            do {
                
                let cars = try JSONDecoder().decode([Car].self, from: data)
                
                completion(.success(cars))
            }
            catch {
                
                print(error)
                completion(.failure(error))
            }
            
        }.resume()
    }
}
