//
//  CarListTableViewCell_v2.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 26.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

@IBDesignable
class CarListTableViewCell: UITableViewCell {

    static let cellIdentifier = "CarListTableViewCellIdentifier"
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelNameLabel: UILabel!
    
    @IBOutlet weak var transmissionImageView: UIImageView!
    @IBOutlet weak var transmissionTypeLabel: UILabel!
    
    @IBOutlet weak var fuelTypeLabel: UILabel!
    
    @IBOutlet weak var fuelLevelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        baseView.layer.cornerRadius = 16
        
        baseView.setShadow()
        
        moreButton.layer.cornerRadius = moreButton.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func decorate(car: Sixt.Car) {
        
        distanceLabel.text = car.distanceLabel
        makeLabel.text = car.make
        modelNameLabel.text = car.modelName
    
        transmissionImageView.image = UIImage(systemName: car.transmissionType.iconImageName)
        transmissionTypeLabel.text = car.transmissionType.description
        
        fuelTypeLabel.text = car.fuelType.description
        fuelTypeLabel.textColor = car.fuelType.color
        fuelLevelLabel.text = "Fuel level \(Int(car.fuelLevel * 100))%"
    }
    
//    deinit {
//        
//        print("\(type(of: self)) deinit")
//    }
}
