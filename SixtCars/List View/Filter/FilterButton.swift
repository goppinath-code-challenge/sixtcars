//
//  FilterButton.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 30.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class FilterButton: UIButton {

    var userSelected = false {
        
        willSet {
            
            prepareUI(userSelected: newValue)
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        layer.cornerRadius = frame.height/2
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func toggle() {
        
        userSelected = !userSelected
    }
    
    func prepareUI(userSelected: Bool) {
        
        if userSelected {
            
            backgroundColor = #colorLiteral(red: 0, green: 0.4216835797, blue: 0.7946787477, alpha: 1)
            setTitleColor(#colorLiteral(red: 0.9558789134, green: 0.9607447982, blue: 0.986558497, alpha: 1), for: .normal)
        }
        else {
            
            backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
            setTitleColor(#colorLiteral(red: 0, green: 0.4216835797, blue: 0.7946787477, alpha: 1), for: .normal)
        }
    }
}

class TransmissionFilterButton: FilterButton {
    
    var transmissionType: Sixt.Car.TransmissionType!
}

class FuelFilterButton: FilterButton {
    
    var fuelType: Sixt.Car.FuelType!
}
