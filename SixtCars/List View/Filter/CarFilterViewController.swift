//
//  CarFilterViewController.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 30.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class CarFilterViewController: UIViewController {

    var carFilter: CarFilter?
    
    @IBOutlet weak var automaticButton: TransmissionFilterButton!
    @IBOutlet weak var manualButton: TransmissionFilterButton!
    
    @IBOutlet weak var petrolButton: FuelFilterButton!
    @IBOutlet weak var dieselButton: FuelFilterButton!
    @IBOutlet weak var electricButton: FuelFilterButton!
    
    var filtered: ((CarFilter?) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        automaticButton.transmissionType = .automatic
        manualButton.transmissionType = .manual
        
        petrolButton.fuelType = .petrol
        dieselButton.fuelType = .diesel
        electricButton.fuelType = .electric
        
        prepareUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        filtered?(carFilter)
    }
    
    private func prepareUI() {
        
        [automaticButton, manualButton].forEach { $0.userSelected = carFilter?.transmissionTypeSet.contains($0.transmissionType) ?? false }
        
        [petrolButton, dieselButton, electricButton].forEach { $0.userSelected = carFilter?.fuelTypeSet.contains($0.fuelType) ?? false }
    }
    
    @IBAction func transmissionOptionTapped(_ sender: TransmissionFilterButton) {
        
        sender.toggle()

        if carFilter?.transmissionTypeSet.contains(sender.transmissionType) ?? false {
            
            carFilter?.transmissionTypeSet.remove(sender.transmissionType)
        }
        else {
            
            if carFilter == nil { carFilter = CarFilter() }
            
            carFilter?.transmissionTypeSet.insert(sender.transmissionType)
        }
    }
    
    @IBAction func fuelOptionTapped(_ sender: FuelFilterButton) {
        
        sender.toggle()

        if carFilter?.fuelTypeSet.contains(sender.fuelType) ?? false {
            
            carFilter?.fuelTypeSet.remove(sender.fuelType)
        }
        else {
            
            if carFilter == nil { carFilter = CarFilter() }
            
            carFilter?.fuelTypeSet.insert(sender.fuelType)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
