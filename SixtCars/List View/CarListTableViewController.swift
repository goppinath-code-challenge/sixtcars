//
//  CarListTableViewController.swift
//  SixtCars
//
//  Created by Goppinath Thurairajah on 24.11.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class CarListTableViewController: UITableViewController {
    
    let emptyDataSourceMessage = "No data available, may be try with different filter options."
    
    @IBOutlet fileprivate var viewModel: CarViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        viewModel.loadCars { [weak self] in
            
            DispatchQueue.main.async {
                
                self?.tableView.reloadData()
                
                self?.tableView.presentEmptyDataSource(message: self?.emptyDataSourceMessage ?? "")
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.numberOfRows(in: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CarListTableViewCell.cellIdentifier, for: indexPath) as! CarListTableViewCell

        let car = viewModel.car(at: indexPath)
        
        // Configure the cell...

        cell.decorate(car: car)
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let car = viewModel.car(at: indexPath)
        
        viewModel.imageCacheManager.loadImage(for: car) { image in
            
            (cell as? CarListTableViewCell)?.previewImageView.image = image
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.destination {
        case let carDetailViewController as CarDetailViewController:
            
            if let indexPathForSelectedRow = tableView.indexPathForSelectedRow {
                
                carDetailViewController.car = viewModel.car(at: indexPathForSelectedRow)
                carDetailViewController.imageCacheManager = viewModel.imageCacheManager
            }
            
        case let carFilterViewController as CarFilterViewController:
            
            carFilterViewController.popoverPresentationController?.delegate = self
            carFilterViewController.carFilter = viewModel.carFilter
            carFilterViewController.filtered = { [unowned self] carFilter in
                
                self.viewModel.carFilter = carFilter
                
                self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
                
                self.tableView.presentEmptyDataSource(message: self.emptyDataSourceMessage)
            }
            
        default: break
        }
    }
    
}

extension CarListTableViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return .none
    }
}
